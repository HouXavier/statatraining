
> 作者：侯新烁 

> **编者按：** 平行趋势假设检验是多期 DID 模型中必不可少的检验步骤，本期为大家介绍在 Stata 中如何进行多期平行趋势假设检验并绘制平行趋势检验图。

> 资料参考来源：[mostly-harmless-replication - 05 Fixed Effects, DD and Panel Data](https://github.com/vikjam/mostly-harmless-replication/blob/master/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data.md)

---

## 1. 导言

双重差分模型（ Difference in Differences , DID ）是政策效果评估中常用的一种计量识别策略。其原理是基于反事实框架来评估政策发生和不发生这两种情景下被观测变量（因变量）的变化，因而样本被划分为实验组（ Treated 或 Treat ）和对照组（ Untreated 或 Control ）。使用该方法的一个尤为重要的假设条件即满足“平行趋势”（ Parallel Trend ），即两组样本在冲击或者政策发生前必须具有可比性，因为控制组的表现被假定为实验组的反事实。本次推文，将结合 `coefplot` 命令为大家介绍多期DID模型平行趋势检验图形的绘制。

## 2. 操作准备

#### 2.1 基本原理图示

在仅有两期的DID模型中，我们通常使用如下图形展示其原理，以 2008 年为政策发生时间点为例：

![两期 DID ](https://images.gitee.com/uploads/images/2018/1204/000019_363da22a_2109590.jpeg "两期DID原理.jpg")

其中，C 对应 Control 组，T 对应 Treat 组，TD 代表 T的反事实情形，则此时 T 与 TD 在 2008 年的差值即为 DID 识别的处理效应，而此时因为前后各仅有一期，实际上较难以识别平行假设是否成立，因而我们通常会对通过样本本身的匹配性加强这一假设的稳健性。

而在多期情形中，图形可能展示如下：

![多期 DID 原理图](https://images.gitee.com/uploads/images/2018/1204/000054_ee0bd323_2109590.jpeg "多期DID原理图.jpg")

多个时间节点信息的出现，使得可以对不同样本中组因变量的变化趋势进行观察，此时，平行趋势要求处理组 T 和控制组 C 在政策时点前具有一致的趋势（平行）。当然，不同组别样本前具有平行趋势，并不代表着政策发生之后的时间段内依然具有反事实意义上的平行趋势。
从文献来看，最为常见的展示是否符合平行趋势假设的检验方法有两个：其一，对比不同组别因变量均值的时间趋势；其二，回归中加入各时点虚拟变量与政策变量的交互项（如本例中年份虚拟变量乘以实验组虚拟变量），若政策或称为处理发生前的交互项系数不显著，则表明的确有着平行趋势。接下来，本推文将给出 Stata 中进行两种检验方法的代码操作和图形输出。


#### 2.2  安装 coefplot
 
 该命令由 Ben Jann （ University of Bern, ben.jann@soz.unibe.ch ）于2017年9月份编写，用于对回归系数和其他结果进行图形绘制，展示系数结果和置信区间范围，更多信息请 `help coefplot` 。若您的电脑尚未安装该命令，请执行如下代码：

```stata
ssc install coefplot, replace
```

*若安装出现问题请使用 findit 命令手动安装

```stata
findit coefplot
```

## 3. 多期 DID 平行趋势图
 
本期推介的是由 vikjam 编写的 Replication of tables and figures from "Mostly Harmless Econometrics" in Stata, R, Python and Julia 中关于 DID 的图形输出部分代码。

#### step 1 准备与数据下载

```stata
clear all
set more off
eststo clear
capture version 14

/* 下载 zip 压缩包数据文件并解压 */
shell curl -o outsourcingatwill_table7.zip "http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip"
unzipfile outsourcingatwill_table7.zip
```

此时，若正常运行，则会在 dos 窗口中看到如图情形。

![下载zip数据](https://images.gitee.com/uploads/images/2018/1204/001057_1b656072_2109590.jpeg "下载zip数据.jpg")


解压后得到文件夹 table7 ，其中的  `autor-jole-2003.dta 数据即为本次示例数据。经验证，部分 Stata 程序可能不能正确执行 dos 下载过程，我们需要将网址 http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip 复制到浏览器进行下载，然后手动解压获得数据文件。

![解压后文件夹](https://images.gitee.com/uploads/images/2018/1204/001122_90b76c45_2109590.jpeg "解压后.jpg")

#### step 2 调用数据并对数据进行预处理


```stata
use "table7/autor-jole-2003.dta", clear

*作者对数据进行了论文的仿制处理，具体经济含义请参考原文，或者请忽略，因为不是敲黑板呦~
/* 对数总就业人数：来自劳工统计局（BLS）就业和收入数据 */
gen lnemp = log(annemp)

/* 非商业服务部门就业：来自美国海关（ CBP ） */
gen nonemp  = stateemp - svcemp
gen lnnon   = log(nonemp)
gen svcfrac = svcemp / nonemp

/* 商业服务部门总就业：来自美国海关（ CBP ） */
gen bizemp = svcemp + peremp
gen lnbiz  = log(bizemp)

/* 生成时间趋势 */
gen t  = year - 78 // 线性时间趋势
gen t2 = t^2       // 二次时间趋势

/* 限制样本时间区间 */
keep if inrange(year, 79, 95) & state != 98

/* 生成更多的统计数据 */
gen clp     = clg + gtc
gen a1624   = m1619 + m2024 + f1619 + f2024
gen a2554   = m2554 + f2554
gen a55up   = m5564 + m65up + f5564 + f65up
gen fem     = f1619 + f2024 + f2554 + f5564 + f65up
gen white   = rs_wm + rs_wf
gen black   = rs_bm + rs_bf
gen other   = rs_om + rs_of
gen married = marfem + marmale

/* 修正工会变量 */
replace unmem = . if inlist(year, 79, 81) 
replace unmem = unmem * 100              
```


#### step 3 估计 DID 模型

数据中，各时间节点与政策处理的交互项已经处理完毕，其中，以 `admico` 开头带 `_`  的表示政策执行前多少期对应的交互项，如 `admico_2` 表示政策发生前 2 期的虚拟变量与处理组变量的交互项；以 `admico` 开头不带 `_`  的表示政策发生后对应交互项，其中 `admico0` 表示政策发生当期。因此，此时可以直接展开多期 DID 回归了；**若我们对该方法进行应用时，需要事先进行交互项的生成**，此处需注意。

![虚拟变量交互项已经生成](https://images.gitee.com/uploads/images/2018/1204/000514_351617dc_2109590.jpeg "虚拟变量交互项已经生成.jpg")


```stata
reg lnths lnemp admico_2 admico_1 admico0 admico1 admico2 admico3 mico4 admppa_2 admppa_1   ///
    admppa0 admppa1 admppa2 admppa3 mppa4 admgfa_2 admgfa_1 admgfa0 admgfa1 admgfa2 admgfa3 ///
    mgfa4 i.year i.state i.state#c.t, cluster(state)
```

该回归通过 `i.year` 、 `i.state` 和 `i.state#c.t` 对时间固定效应、个体固定效应和个体与时间趋势的交叉效应进行了控制。

![回归结果展示](https://images.gitee.com/uploads/images/2018/1204/000445_042a6e9d_2109590.jpeg "回归结果.jpg")


#### step 4 图形的输出与优化

使用 `coefplot` 可较为便捷和快速的生成多期动态效应图，更多使用方法请 `help coefplot` 。

```stata
coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)   vertical  addplot(line @b @at) 
```

![多期图](https://images.gitee.com/uploads/images/2018/1204/000654_db772162_2109590.jpeg "多期图.jpg")

通过 coefplot 选项对图形进行优化，对横纵坐标的标记、标题以及样式等进行设定：

```stata
coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)                     ///
          coeflabels(admico_2 = "2 yr prior"                                                ///
         admico_1 = "1 yr prior"                                                ///
         admico0  = "Yr of adopt"                                               ///
         admico1  = "1 yr after"                                                ///
         admico2  = "2 yr after"                                                ///
         admico3  = "3 yr after"                                                ///
         mico4    = "4+ yr after")                                              ///
         vertical                                                                          ///
         yline(0)                                                                          ///
         ytitle("Log points")                                                              ///
         xtitle("Time passage relative to year of adoption of implied contract exception") ///
         addplot(line @b @at)                                                              ///
         ciopts(recast(rcap))                                                              ///
         rescale(100)                                                                      ///
         scheme(s1mono)
```

![多期图优化](https://images.gitee.com/uploads/images/2018/1204/000635_f886cbce_2109590.jpeg "多期图优化.jpg")


当然，也可以通过 scheme() 将风格更改为自己熟悉或常用的格式，例如 qleanmono 风格：

![多期图优化qleanmono](https://images.gitee.com/uploads/images/2018/1204/000717_987a1ad5_2109590.jpeg "多期图优化qleanmono.jpg")

最后，图片的输出保存：

```stata
graph export "figure.png", replace
```

## 4. 代码汇总

```stata
ssc install coefplot, replace
*findit coefplot

clear all
set more off
eststo clear
capture version 14

shell curl -o outsourcingatwill_table7.zip "http://economics.mit.edu/~dautor/outsourcingatwill_table7.zip"
unzipfile outsourcingatwill_table7.zip

use "table7/autor-jole-2003.dta", clear
gen lnemp = log(annemp)

gen nonemp  = stateemp - svcemp
gen lnnon   = log(nonemp)
gen svcfrac = svcemp / nonemp

gen bizemp = svcemp + peremp
gen lnbiz  = log(bizemp)

gen t  = year - 78
gen t2 = t^2      

keep if inrange(year, 79, 95) & state != 98

gen clp     = clg + gtc
gen a1624   = m1619 + m2024 + f1619 + f2024
gen a2554   = m2554 + f2554
gen a55up   = m5564 + m65up + f5564 + f65up
gen fem     = f1619 + f2024 + f2554 + f5564 + f65up
gen white   = rs_wm + rs_wf
gen black   = rs_bm + rs_bf
gen other   = rs_om + rs_of
gen married = marfem + marmale

replace unmem = . if inlist(year, 79, 81) 
replace unmem = unmem * 100              

reg lnths lnemp admico_2 admico_1 admico0 admico1 admico2 admico3 mico4 admppa_2 admppa_1   ///
    admppa0 admppa1 admppa2 admppa3 mppa4 admgfa_2 admgfa_1 admgfa0 admgfa1 admgfa2 admgfa3 ///
    mgfa4 i.year i.state i.state#c.t, cluster(state)

coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)   vertical  addplot(line @b @at) 


coefplot, keep(admico_2 admico_1 admico0 admico1 admico2 admico3 mico4)                     ///
          coeflabels(admico_2 = "2 yr prior"                                                ///
         admico_1 = "1 yr prior"                                                ///
         admico0  = "Yr of adopt"                                               ///
         admico1  = "1 yr after"                                                ///
         admico2  = "2 yr after"                                                ///
         admico3  = "3 yr after"                                                ///
         mico4    = "4+ yr after")                                              ///
         vertical                                                                          ///
         yline(0)                                                                          ///
         ytitle("Log points")                                                              ///
         xtitle("Time passage relative to year of adoption of implied contract exception") ///
         addplot(line @b @at)                                                              ///
         ciopts(recast(rcap))                                                              ///
         rescale(100)                                                                      ///
         scheme(s1mono)

graph export "figurename.png", replace

```


> 后记：本质上是根据平行假设检验的回归设计，获得系数和对应标准误信息，然后利用标记置信区间的 connected 进行绘图。
