> 作者：侯新烁 ( [码云](https://gitee.com/HouXavier/statatraining)) 

> **编者按：** 在计量学习以及数据处理和论文写作过程中，我们常常需要对不同文件和不同项目创建不同文件夹加以区分和管理，笔者在使用stata进行回归分析和程序处理时也时常需要新建文件夹，导致经常需要在窗口内进行切换和鼠标新建操作。本次推文为大家介绍一个新近编写的小程序 efolder ，用于快速生成文件夹和/或子文件夹，并默认将stata工作路径设置为新生成文件夹并在结果窗口提示其连接地址。

> 程序作者：Hou Xinshuo （houxinshuo@126.com）

---

## 1. efolder说明
`efolder` -- **easy folder** ，为更加快速和便捷生成文件夹而生。在特定的情况下，我们需要重复或一次生成多个文件夹，并进行快速查看，基于 `mkdir` 笔者编写了一个文件夹创建小程序。

- 命令如下：
```
 efolder [foldername] ,  [cd()] [sub] [nochange] [subname()]
```

其中，为便于使用，可简写为 `ef` 命令，当有新文件加生成，同时也会将当前工作路径设置到该文件夹； `[foldername]` 用于定义特定工作路径 （ 可通过选项 `cd()` 加以设定，可选 ）下要生成的文件夹名称（名称可包含空格）； `cd()` 选项可定义非当前工作路径中的位置;  `[subname()]` 用于设定子文件名称，如书写为 `sub(1 j k 25)` 时，将生成 1 、 j 、 k 和 25 四个文件夹； `[nochange]` 可简写为 `noc`，当该选项设置时，程序将不会改变 `efolder` 命令运行前的工作路径。

## 2. 下载方法 
在 Stata 命令窗口中输入如下命令即可安装该命令：

- 第一种： (Note：网络状况不好可能提示服务器链接超时)
```stata
. ssc install efolder, replace
```

- 第二种：(Note： 亲测可顺畅安装)
```stata
. ssc des e
. net install efolder, replace 
```

或输入 `findit efolder` 命令，或 `search efolder`  命令,在弹出页面中按指引点击链接下载即可。


## 3. 应用范例：

#### （1）无设定 

```stata
. ef
```
**结果展示:**

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/145148_8cdb0f25_2109590.png "ef.png")


其中，蓝色部分表示为超链接，可通过鼠标点击打开相应文件夹。当不做任何设定时，将输出当前工作路径。

（此路径将因不同安装位置和 `profile` 设定的不同而有差异，推文中以笔者电脑为例展示。）

#### （2）设定路径

```stata
.  efolder, cd(D:\stata15\hxs\连享会007)
```
**结果展示:**

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/145610_08424bcc_2109590.png "efcd.png")

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/145620_32c65627_2109590.png "efcd2.png")

当目的路径不存在时，`ef` 将生成该文件夹，并将工作路径更改至 `cd()` 提供的文件夹路径。

#### （3）设定路径与子文件夹

```stata
.  efolder, cd(D:\stata15\hxs\连享会007) sub(侯新烁 连玉君 007小组1号成员 007小组2号成员)
```

**结果展示:**

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/145951_6f2f0873_2109590.png "efcdsub.png")

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150031_5fec9097_2109590.png "efcdsub2.png")

可通过 `sub()` 选项在指定路径下生成子文件夹，Note：子文件夹名称以空格分割。

#### （4）设定路径、文件夹与子文件夹

```stata
.  efolder 新成员 2018, cd(D:\stata15\hxs\连享会007) sub(新1 新2 新3)
```

**结果展示:**

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150404_182b5655_2109590.png "efncdsub.png")

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150412_9e592119_2109590.png "efncdsub2.png")

当设定 `[foldername]` 时（可含有空格），将在指定 `cd`下生成该文件夹并将工作路径更改到此位置，`sub()` 将在刚刚生成的文件夹内（在本例中即在文件夹 **新成员 2018** 内）生成相应子文件夹。

#### （5）不更改工作路径

```stata
.  efolder 新成员 2019, cd(D:\stata15\hxs\连享会007) sub(明1 明2 明3) noc
```

**结果展示:**

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150817_9abe16ba_2109590.png "efno.png")

将生成新的文件夹和子文件夹

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150837_6daa865c_2109590.png "efno2.png")

但工作路径仍为本条命令执行前的工作路径，即 `nochange` 使得命令不改变之前的工作路径。

> ![输入图片说明](https://images.gitee.com/uploads/images/2018/1026/150908_08cc8ff4_2109590.png "efno3.png")


## 4. 在循环中使用的范例：

```stata
foreach f in 侯新烁 连玉君 王小二 小黄鸡 小黄鸭 {

   ef `f', cd(D:\stata15\hxs\连享会007\newproject) ///

           sub(待完成 已完成 原始数据 程序) noc

}
```

可以通过循环的方式，依次按照成员生成文件夹，并设置相应子文件夹。
并可以与其他相关命令，如数据和文档的存储命令 `save` 、 `erase` 等配合使用。


## 5. 代码汇总：
```stata
. ef

. ef, cd(D:\stata15\hxs\连享会007)

. ef, cd(D:\stata15\hxs\连享会007) sub(侯新烁 连玉君 007小组1号成员 007小组2号成员)

. ef 新成员 2018, cd(D:\stata15\hxs\连享会007) sub(新1 新2 新3)

. ef 新成员 2019, cd(D:\stata15\hxs\连享会007) sub(明1 明2 明3) noc

. foreach f in 侯新烁 连玉君 王小二 小黄鸡 小黄鸭 {

   ef `f', cd(D:\stata15\hxs\连享会007\newproject) ///

           sub(待完成 已完成 原始数据 程序) noc

}
```


> 后记： **在do文件里就能easy的管理文件夹创建啦~~~**


>#### 关于我们![请联系](https://images.gitee.com/uploads/images/2018/1107/200128_b1c1c7f9_2109590.jpeg "助理.jpg")



